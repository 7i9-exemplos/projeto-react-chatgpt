import reactLogo from './assets/react.svg'
import './App.css'
import { Configuration, OpenAIApi } from 'openai'
import { useState } from 'react';
function App() {

const [nome,setNome] = useState()
const [resposta,setResposta]:any = useState('')
const configuration = new Configuration({
  apiKey: "insira a sua chave da api",
});
const openai = new OpenAIApi(configuration);
  function alterarValor(event:any)
  {   
    const value = event.target.value
    setNome(value)
  }
  async function busca() {  
  const response = await openai.createCompletion({
  model: "text-davinci-003",
  prompt: nome,
  temperature: 0,
  max_tokens: 100,
  top_p: 1,
  frequency_penalty: 0.2,
  presence_penalty: 0,
});
  setResposta(response.data.choices[0].text)
  }
  
  return (
    <>
      <div>
       
        <a href="https://react.dev" target="_blank">
          <img src={reactLogo} className="logo react" alt="React logo" />
        </a>
      </div>
     
      <div className="card">
       
        <div className="field">
  <div className="">
            <input onChange={alterarValor} value={nome} name={'nome'} className='textarea is-medium is-primary' type='textarea' />
          </div>
        </div>
         <div className="field">
  <div className="control">
            <label>{ resposta }</label>
          </div>
          </div>
        <div className="field is-grouped">
  <div className="control">
        <button className='button is-primary' onClick={() => busca()}>
       Buscar
            </button>
          </div>
          </div>
        
      </div>
      <p className="read-the-docs">
        Digite para fazer a busca
      </p>
    </>
  )
}

export default App
